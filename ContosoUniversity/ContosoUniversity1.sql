CREATE DATABASE ContosoUniversity1
GO
USE  ContosoUniversity1
GO 
CREATE TABLE Student 
(
ID INT NOT NULL PRIMARY KEY,
LastName NVARCHAR(100) NOT NULL,
FirstMidName NVARCHAR(100) NOT NULL,
EnrollmentDate DATE NOT NULL, 
)
GO
CREATE TABLE Course
(
CourseID INT NOT NULL PRIMARY KEY,
Title NVARCHAR(100) NOT NULL,
Credits INT NOT NULL,
)
GO
CREATE TABLE Enrollment
(
EnrollmentID  INT NOT NULL PRIMARY KEY,
CourseID INT NOT NULL,
Grade NVARCHAR(100) NOT NULL,
)
GO
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Carson','Alexander','DateTime.Parse("2005-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Meredith','Alonso','DateTime.Parse("2002-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Arturo','Anand','DateTime.Parse("2003-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Gytis','Barzdukas','DateTime.Parse("2002-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Yan','Li','DateTime.Parse("2002-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Peggy','Justice','DateTime.Parse("2001-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Laura','Norman','DateTime.Parse("2003-09-01")')
INSERT INTO Student(LastName,FirstMidName,EnrollmentDate) VALUES ('Nino','Olivetto','DateTime.Parse("2005-09-01")')
GO
INSERT INTO Course(CourseID,Title,Credits) VALUES (1050,'Chemistry', 3)
INSERT INTO Course(CourseID,Title,Credits) VALUES (4022,'Microeconomics', 3)
INSERT INTO Course(CourseID,Title,Credits) VALUES (4041,'Macroeconomics', 3)
INSERT INTO Course(CourseID,Title,Credits) VALUES (1045,'Calculus', 4)
INSERT INTO Course(CourseID,Title,Credits) VALUES (3141,'Trigonometry', 4)
INSERT INTO Course(CourseID,Title,Credits) VALUES (2021,'Composition', 3)
INSERT INTO Course(CourseID,Title,Credits) VALUES (2042,'Literature', 4)
GO
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (1,1050,'Grade.A')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (1,4022,'Grade.C')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (1,4041,'Grade.B')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (2,1045,'Grade.B')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (2,3141,'Grade.F')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (2,2021,'Grade.F')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (3,1050)
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (4,1050)
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (4,4022,'Grade.F')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (5,4041,'Grade.C')
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (6,1050)
INSERT INTO Enrollment(EnrollmentID,CourseID,Grade) VALUES (7,3141,'Grade.A')

SELECT * FROM Course
SELECT * FROM Student
SELECT * FROM Enrollment